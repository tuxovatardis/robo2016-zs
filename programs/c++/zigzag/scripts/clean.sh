#!/bin/bash
cd "$(dirname "$(realpath "$0")")/..";
rm -f CMakeCache.txt cmake_install.cmake
rm -rf CMakeFiles/ out/
