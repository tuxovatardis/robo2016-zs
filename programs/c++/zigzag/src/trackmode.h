//
// Tracking mode
//

#ifndef ZIGZAG_TRACKMODE_H
#define ZIGZAG_TRACKMODE_H
namespace tux {
	/**
	 * Tracking mode (i.e. which side of line shall the robot follow).
	 */
	enum trackmode {
		left, right
	};
}
#endif //ZIGZAG_TRACKMODE_H
