//
// User interaction methods
//

#ifndef ZIGZAG_UI_H
#define ZIGZAG_UI_H

#include "trackmode.h"
#include <display.h>

using namespace robofat;

namespace tux {
	/**
	 * \brief User interaction related methods.
	 */
	class ui {
	public:
		ui(display &disp);

		/**
		 * \brief Show tracking mode selection menu and return the selected option.
		 *
		 * @return   Selected tracking mode.
		 */
		trackmode getTrackMode();

		/**
		 * \brief Wait for user's run confirmation.
		 *
		 * @return Whether the program should start (true) or end (false).
		 */
		bool waitStart();

		/**
		 * \brief Wait for escape press to end the program.
		 */
		void waitExit();

	private:
		display &disp;
	};
}

#endif //ZIGZAG_UI_H
