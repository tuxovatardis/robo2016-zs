//
// Program initializer
//

#include <iostream>
#include <led.h>
#include "ui.h"
#include <settings.h>
#include "mover.h"

using namespace std;
using namespace tux;
using namespace robofat;

/**
 * \brief Second thread entry point, steps int the mover main loop
 */
void *run(void *arg) {
	mover *ptr = (mover *) arg;
	ptr->run();
	pthread_exit(NULL);
}

/**
 * \brief Create new thread for mover and start it
 */
pthread_t start(const mover &program) {
	pthread_t thread;
	pthread_attr_t attrib;

	pthread_attr_init(&attrib);
	// allow join
	pthread_attr_setdetachstate(&attrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thread, &attrib, run, (void *) &program);
	pthread_attr_destroy(&attrib);
	return thread;
}

/**
 * \brief Stop the second thread and wait for its exit
 */
void quit(mover &program, pthread_t &thread) {
	void *nothing;

	program.setStopFlag(true);
	pthread_join(thread, &nothing);
}

/**
 * \brief Main function
 */
int main() {
	// set led color and print info
	led_set(led_color::YELLOW, led_color::YELLOW);
	display scr;
	scr.init_log("start");

	// allocate and construct classes on the stack
	ui user(scr);
	settings config;

	// get side of line to track from user
	trackmode mode = user.getTrackMode();

	// load settings backing store from config file
	ifstream input;
	input.open("/etc/tux.conf");
	config.load(input, true, true);
	input.close();

	// allocate and construct mover and place the sensor
	mover mover(config, mode, scr);
	mover.placeSensor();

	// set led color and wait for user confirmation
	led_set(led_color::GREEN, led_color::GREEN);
	if (!user.waitStart())
		return 2;

	// start new thread and wait for its exit
	pthread_t thread = start(mover);
	user.waitExit();
	quit(mover, thread);

	// cleanup, exit
	led_off();
	pthread_exit(NULL);
}
