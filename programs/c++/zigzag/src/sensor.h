//
// Sensor arm
//

#ifndef ZIGZAG_SENSOR_H
#define ZIGZAG_SENSOR_H

#include <ev3dev.h>
#include <display.h>

using namespace robofat;

namespace tux {
	/**
	 * \brief Sensor arm.
	 */
	class sensor {
	public:
		/**
		 * \brief Construct new sensor arm.
		 *
		 * @param read    Port with light sensor connected.
		 * @param write   Port with medium motor connected.
		 * @param scr     Display
		 */
		sensor(ev3dev::address_type read, ev3dev::address_type write, display &scr);

		/**
		 * \brief Deallocate sensors.
		 */
		~sensor();

		/**
		 * \brief Get light value.
		 *
		 * @return Value in range 0-100 (%).
		 */
		int getLight();

		/**
		 * \brief Set sensor motor power.
		 *
		 * @param power Sensor power in range -100 to 100 (%).
		 */
		void setPower(int power);

		/**
		 * \brief Get sensor motor tacho.
		 *
		 * @return Sensor motor tacho in degrees.
		 */
		int getTacho();

		/**
		 * \brief Reset the sensor motor (and reset the tachometer)
		 */
		void resetTacho();

		/**
		 * \brief Stop the sensor motor.
		 */
		void stop();

		/**
		 * \brief Start the sensor motor movement.
		 */
		void run();

	private:
		ev3dev::color_sensor *color; ///< Color sensor for data input
		ev3dev::medium_motor *motor; ///< Medium motor for data output
	};
}
#endif //ZIGZAG_SENSOR_H
