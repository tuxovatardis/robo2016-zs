//
// Created by kuba on 10.4.16.
//

#include <unistd.h>
#include "mover.h"
#include <util.h>
using namespace robofat::utility;

tux::mover::mover(settings &set, const trackmode mode, display &disp)
// initialize from settings
		: pid_wheels(set.get_float("pid2 wheels kp"),
		             set.get_float("pid2 wheels ki"),
		             set.get_float("pid2 wheels kd"),
		             set.get_float("pid2 wheels filter arg1"),
		             set.get_float("pid2 wheels filter arg2")),
		  pid_sensor(set.get_float("pid2 sensor kp"),
		             set.get_float("pid2 sensor ki"),
		             set.get_float("pid2 sensor kd"),
		             set.get_float("pid2 sensor filter arg1"),
		             set.get_float("pid2 sensor filter arg2")),
		  control(set.get_port("left motor port"), set.get_port("right motor port"), &disp),
		  arm(set.get_port("sensor port"),
		      set.get_port("sensor motor port"), disp),
		  runFlag(true) {
	sensorMin = set.get_integer("pid2 sensor min");
	sensorMax = set.get_integer("pid2 sensor max");

	lightTarget = set.get_integer("pid2 sensor target");
	sensorCenter = set.get_integer("effect sensor offset");
	targetCoef = set.get_float("pid2 target coef");
	overflowCoef = set.get_float("pid2 overflow coef");
	sleep = set.get_integer("pid2 sleep");
	control.set_base_speed(set.get_integer("pid2 wheels speed"));
	control.set_max_speed(set.get_integer("pid2 max speed"));
	control.enable_clip_overflow(set.get_bool("pid2 max speed flow"));
	control.set_left_invert(set.get_bool("left wheel inverted"));
	control.set_right_invert(set.get_bool("right wheel inverted"));
	trackRight = mode == trackmode::right;
}

void tux::mover::run() {

	// initialize sensor arm
	arm.setPower(0);
	arm.run();
	// initialize controller
	control.push(0);
	// allocate & initialize timestamps
	timespec lastWait;
	clock_gettime(CLOCK_MONOTONIC, &lastWait);
	timespec lastMeasure = lastWait; // copy
	// while the program should run
	while (runFlag) {
		float diff = calc_delta_us(lastMeasure) / 1000.0f;
		if (diff <= 0)
			diff = 0.1; // 0.1 ms
		// get data
		int light = arm.getLight();
		int tacho = arm.getTacho();

		// process data
		int lightError = lightTarget - light;
		int sensorPower = doSensor(lightError, diff);
		int target = tacho + (int) roundf(sensorPower * targetCoef);
		int robot_ccw = calcRobot(target, diff);

		control.push(robot_ccw);

		// wait to next iteration
		long deltatime = calc_delta_us(lastWait);
		long wait = sleep * 1000 - deltatime;
		if (wait > 0)
			usleep((useconds_t) wait);
	}
	control.stop();
	arm.stop();
}

void tux::mover::setStopFlag(bool stop) {
	runFlag = !stop;
}

int tux::mover::doSensor(int lightError, float dt) {
	int power = (int) roundf(pid_sensor.process(lightError, dt));
	if (trackRight)
		power *= -1;
	arm.setPower(power);
	return power;
}

int tux::mover::calcRobot(int target, float dt) {
	int error = target - sensorCenter;
	int overflow = getSensorOverflow(target);
	if (overflow != 0) {
		error += overflow * overflowCoef;
	}
	float ccw = pid_wheels.process(error, dt);
	return (int) roundf(ccw);
}

int tux::mover::getSensorOverflow(int destination) {
	int sensor_overflow = 0;
	if (destination > sensorMax) {
		sensor_overflow = destination - sensorMax;
	} else if (destination < sensorMin) {
		sensor_overflow = destination - sensorMin;
	}
	return sensor_overflow;
}

void tux::mover::placeSensor() {
	static constexpr useconds_t WAIT = 1000000; // 500 ms
	// align to the right side
	arm.setPower(-50);
	arm.run();
	usleep(WAIT);
	arm.resetTacho();
	// go to the left side
	if (!trackRight) {
		arm.setPower(50);
		arm.run();
		usleep(WAIT);
		arm.stop();
	}
}

