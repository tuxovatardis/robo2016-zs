//
// Main loop
//

#ifndef ZIGZAG_MOVER_H
#define ZIGZAG_MOVER_H

#include <atomic>
#include "sensor.h"
#include "trackmode.h"
#include <pid.h>
#include <steer_pilot.h>
#include <settings.h>

using namespace robofat;

namespace tux {
	/**
	 * \brief Main program algorithm
	 */
	class mover {
	public:
		/**
		 * \brief Construct new mover instance
		 *
		 * @param set   Configuration
		 * @param mode  Tracking mode
		 * @param scr   Display
		 */
		mover(settings &set, const trackmode mode, display &scr);

		/**
		 * \brief Run the main loop
		 */
		void run();

		/**
		 * \brief Set the stop flag (the run flag is inverse)
		 *
		 * @param stop Stop flag
		 */
		void setStopFlag(bool stop);

		/**
		 * \brief Set sensor motor tacho to the right value and place the sensor to the correct location.
		 */
		void placeSensor();

	private:
		sensor arm; ///< Sensor controller
		pid pid_sensor; ///< PID for sensor arm
		pid pid_wheels; ///< PID for robot
		steer_pilot control; ///< Wheels differential controller
		int sensorMin; ///< Minimal tacho sensor limit (for overflow detection)
		int sensorMax; ///< Maximal tacho sensor limit (for underflow detection)
		std::atomic_bool runFlag; ///< Whether the main loop should continue

		int lightTarget; ///< Target light value
		int sensorCenter; ///< Target sensor angle
		float targetCoef; ///< Conversion coefficient for adding actual change to old tacho value
		float overflowCoef; ///< Coefficient for converting overflowed sensor angle to additional robot error
		long sleep; ///< How long the algorithm should sleep between iterations (in ms)
		bool trackRight; ///< Whether we track right (true) of left (false) side of line

		/**
		 * \brief Process light error by the sensor arm
		 *
		 * @param lightError   Light error
		 * @param dt           Time from last iteration
		 * @return             New sensor motor power
		 */
		int doSensor(int lightError, float dt);

		/**
		 * \brief Calculate new wheels difference
		 *
		 * @param target    Sensor arm tacho
		 * @param dt        Time from the last iteration
		 * @return          Difference between wheels
		 */
		int calcRobot(int target, float dt);

		/**
		 * \brief Get how much the sensor surpasses its min/max position
		 *
		 * @param destination   Sensor arm tacho
		 * @return              Overflow
		 */
		int getSensorOverflow(int destination);
	};
}

#endif //ZIGZAG_MOVER_H
