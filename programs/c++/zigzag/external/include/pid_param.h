/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID data structures.
 */

#ifndef ROBOFAT_PID_PARAMS_H
#define ROBOFAT_PID_PARAMS_H

namespace robofat {
	/**
	 * \brief PID constants
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	template<typename constant>
	struct pid_consts {
		/**
		 * \brief Proportional constant (Kp)
		 */
		constant kp;
		/**
		 * \brief Integral constant (Ki)
		 */
		constant ki;
		/**
		 * \brief Derivative constant (Kd)
		 */
		constant kd;
	};

	/**
	 * \brief Weighted moving average filter parameters
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	template<typename constant>
	struct pid_filter {
		/**
		 * \brief New data weight
		 */
		constant actualWeight;
		/**
		 * \brief History weight
		 */
		constant oldWeight;
	};

	/**
	 * \brief PID state parameters
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	template<typename variable>
	struct pid_state {
		/**
		 * \brief Actual integral value
		 */
		variable integral;
		/**
		 * \brief Last processed error
		 */
		variable lastError;
		/**
		 * \brief Last derivative input
		 */
		variable history;
	};

	/**
	 * \brief PID state parameters - extended float version
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
     */
	template<>
	struct pid_state<float> {
		/**
		 * \brief Actual integral value
		 */
		float integral;
		/**
		 * \brief Last processed error
		 */
		float lastError;
		/**
		 * \brief Last derivative input
		 */
		float history;

		/**
		 * \brief Set structure to it's clean state
		 */
		void reset() {
			integral = 0.0f;
			lastError = std::nanf("");
			history = std::nanf("");
		}
	};

	/**
	 * \brief PID state parameters - extended double version
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	template<>
	struct pid_state<double> {
		/**
		 * \brief Actual integral value
		 */
		double integral;
		/**
		 * \brief Last processed error
		 */
		double lastError;
		/**
		 * \brief Last derivative input
		 */
		double history;

		/**
		 * \brief Set structure to it's clean state
		 */
		void reset() {
			integral = 0.0;
			lastError = std::nan("");
			history = std::nan("");
		}
	};
}
#endif //ROBOFAT_PID_PARAMS_H
