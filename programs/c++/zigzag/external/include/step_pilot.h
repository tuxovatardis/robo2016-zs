/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * TuxovaTARDIS' Mechanics-style implementation of robot's physics - a stepped pilot.
 */

#ifndef ROBOFAT_STEP_PILOT_H
#define ROBOFAT_STEP_PILOT_H

#include <chrono>
#include "ev3dev.h"
#include "pid_param.h"
#include "pid.h"

#ifdef HAS_CURSES

#include "display.h"

#endif

using ev3dev::address_type;
using ev3dev::motor;
using ev3dev::large_motor;
using ev3dev::gyro_sensor;
using ev3dev::touch_sensor;

namespace robofat {
	/**
	 * \brief Step pilot
	 *
	 * TuxovaTARDIS' Mechanics-style implementation of robot's physics.
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	class step_pilot {
	public:
		/**
		 * \brief Arc distance specification mode.
		 */
		enum arc_mode {
			/**
			 * \brief Argument specified is the distance.
			 */
					distance,
			/**
			 * \brief Argument specified is the angle.
			 */
					angle
		};

#ifdef HAS_CURSES

		/**
		 * \brief Initializes new stepped pilot
		 *
		 * @param left_motor        Left motor port
		 * @param right_motor       Right motor port
		 * @param touch             Touch sensor port, NULL is allowed -> align won't be available
		 * @param gyro              Gyro sensor port, NULL is allowed -> corrected turn won't be available
		 * @param track_width       Distance between wheel centers
		 * @param wheel_diameter    Diameter of both wheels (must be the same)
		 * @param scr               Display for Display for the question for user. The user won't be asked for action when motors aren't connected if this is nullptr.
		 * @throws device_not_found If user cannot be prompted for action and the motors aren't connected.
		 */
		step_pilot(address_type &left_motor, address_type &right_motor, address_type *touch, address_type *gyro,
		           float track_width, float wheel_diameter, display *scr);

#else
		/**
		 * \brief Initializes new stepped pilot
		 *
		 * @param left_motor       Left motor port
		 * @param right_motor      Right motor port
		 * @param touch            Touch sensor port, NULL is allowed -> align won't be available
		 * @param gyro             Gyro sensor port, NULL is allowed -> corrected turn won't be available
		 * @param track_width      Distance between wheel centers
		 * @param wheel_diameter   Diameter of both wheels (must be the same)
		 * @throws device_not_found When the motors aren't connected.
		 */
		step_pilot(address_type &left_motor, address_type &right_motor, address_type *touch, address_type *gyro,
				   float track_width, float wheel_diameter);
#endif

		/**
		 * \brief Destructs this stepped pilot
		 *
		 * Deallocates motors and sensors
		 */
		~step_pilot();

		/**
		 * \brief Move the robot.
		 *
		 * @param distance      Distance in units to travel.
		 * @param wait          Whether this function is going to block until the move finishes.
		 */
		void move(int distance, bool wait = true);

		/**
		 * \brief Rotate the robot by the specified angle.
		 *
		 * @param degrees_ccw   Rotation in degrees counterclockwise.
		 * @param wait          Whether this function is going to block until the turn finishes.
		 */
		void turn_only(int degrees_ccw, bool wait = true);

		/**
		 * \brief Rotate the robot by the specified angle + use a stepped gyro fix.
		 *
		 * If the gyro is not available, this is equivalent to {@code turn_only(degrees_ccw, true);} call.
		 * This function always blocks if {@code degrees_ccw != 0}
		 *
		 * @param degrees_ccw   Rotation in degrees counter-clockwise.
		 * @param max_error     Maximum error for fix.
		 */
		void turn_p_step(int degrees_ccw, int max_error);

		/**
		 * \brief Rotate the robot by the specified angle + gyro speed control PID.
		 *
		 * If the gyro is not available, this is equivalent to {@link step_pilot#turn_only} call.
		 * This function always blocks if {@code degrees_ccw != 0}
		 *
		 * @param degrees_ccw   Rotation in degrees counter-clockwise.
		 * @param max_error     Maximum error for fix.
		 */
		void turn_pid_linear(int degrees_ccw, int max_error);

		/**
		 * \brief Move the robot by the arc.
		 *
		 * Negative radius means clockwise rotation.
		 *
		 * @param spec          Distance in units or angle in degrees, see mode.
		 * @param radius        The radius of the middle arc. Negative radius means clockwise rotation.
		 * @param mode          Whether the spec argument is specifying distance or angle.
		 * @param wait          Whether this function is going to block until the robot is stopped.
		 */
		void turn_arc_only(int spec, int radius, step_pilot::arc_mode mode, bool wait = true);

		/**
		 * \brief Start moving the robot forward.
		 */
		void forward();

		/**
		 * \brief Start moving the robot backward.
		 */
		void backward();

		/**
		 * \brief Stop the robot.
		 *
		 * @param wait          Whether this function is going to block until the robot is stopped.
		 */
		void stop(bool wait = true);

		/**
		 * \brief Move the robot to the wall using the touch sensor and stop.
		 *
		 * When the touch sensor is not available this function is a no-op.
		 */
		void move_to_wall();

		/**
		 * \brief Calibrate the gyroscopic sensor if available.
		 *
		 * When the gyroscopic sensor is not available this function is a no-op.
		 * Note: the robot/sensor must be still during the calibration.
		 */
		void gyro_calibrate();

		/**
		 * \brief Get the velocity of moves.
		 *
		 * This is in the specified units.
		 *
		 * @return              The velocity of the robot in units/s.
		 */
		int get_speed_move();

		/**
		 * \brief Set the velocity of moves.
		 *
		 * This is in the specified units.
		 *
		 * @param un_per_sec    The velocity of the robot in units/s.
		 */
		void set_speed_move(int un_per_sec);

		/**
		 * \brief Get the angular velocity of turns.
		 *
		 * This is in the degrees per second units.
		 *
		 * @return              The angular velocity of the robot in °/s.
		 */
		int get_speed_turn();

		/**
		 * \brief Set the angular velocity of turns.
		 *
		 * This is in the degrees per second units.
		 *
		 * @param deg_per_sec   The angular velocity of the robot in °/s.
		 */
		void set_speed_turn(int deg_per_sec);

		/**
		 * \brief Get whether the left wheel's signal is inverted.
		 *
		 * @return              {@code true} if it's inverted, {@code false} otherwise.
		 */
		bool get_left_invert();

		/**
		 * \brief Get whether the right wheel's signal is inverted.
		 *
		 * @return              {@code true} if it's inverted, {@code false} otherwise.
		 */
		bool get_right_invert();

		/**
		 * \brief Set whether the left wheel's signal is inverted.
		 *
		 * @param invert        {@code true} if it's inverted, {@code false} otherwise.
		 */
		void set_left_invert(bool invert);

		/**
		 * \brief Set whether the right wheel's signal is inverted.
		 *
		 * @param invert        {@code true} if it's inverted, {@code false} otherwise.
		 */
		void set_right_invert(bool invert);

		/**
		 * \brief Get the position PID constants from ev3dev kernel.
		 *
		 * @return              PID constants used for holding position.
		 */
		pid_consts<int> get_position_pid();

		/**
		 * \brief Get the speed PID constants from ev3dev kernel.
		 *
		 * @return              PID constants used for speed control.
		 */
		pid_consts<int> get_speed_pid();

		/**
		 * \brief Set the position PID constants for ev3dev kernel.
		 *
		 * @param constants     PID constants used for holding position.
		 */
		void set_position_pid(pid_consts<int> &constants);

		/**
		 * \brief Set the speed PID constants for ev3dev kernel.
		 *
		 * @param constants     PID constants used for speed control.
		 */
		void set_speed_pid(pid_consts<int> &constants);

		/**
		 * \brief Get ramp up time (i.e. after what time is the maximum power applied)
		 *
		 * @return              Ramp up time in milliseconds.
		 */
		int get_ramp_up();

		/**
		 * \brief Get ramp down time  (i.e. after what time is the minimum power applied)
		 *
		 * @return              Ramp down time in milliseconds.
		 */
		int get_ramp_down();

		/**
		 * \brief Set ramp up time (i.e. after what time is the maximum power applied)
		 *
		 * @param msec          Ramp up time in milliseconds.
		 */
		void set_ramp_up(int msec);

		/**
		 * \brief Set ramp down time (i.e. after what time is the maximum power applied)
		 *
		 * @param msec          Ramp down time in milliseconds.
		 */
		void set_ramp_down(int msec);

		/**
		 * \brief Get the proportional constant used for step-fixed rotation.
		 *
		 * @return              Kp of P regulator.
		 */
		float get_turn_step_kp();

		/**
		 * \brief Set the proportional constant used for step-fixed rotation.
		 *
		 * @param value         Kp of P regulator.
		 */
		void set_turn_step_kp(float value);

		/**
		 * \brief Get the PID regulator used for linear-controlled rotation.
		 *
		 * @return              Modifiable PID controller.
		 */
		pid get_turn_linear_pid();

	private:
		/**
		 * \brief Speed mode.
		 */
		enum speed_mode {
			/**
			 * \brief Straight mode
			 */
					move_mode,
			/**
			 * \brief Turn mode
			 */
					turn_mode
		};

		/**
		 * \brief Apply the configured speed to the motors.
		 *
		 * @param left_reverse  Whether the left motor should go backward.
		 * @param right_reverse Whether the right motor should go backward.
		 * @param mode          Whether the move velocity or the turn velocity is applied.
		 */
		void push_speed(bool left_reverse, bool right_reverse, const speed_mode mode = move_mode);

		/**
		 * \brief Apply the specified speed to the motors.
		 *
		 * @param value         Speed to push in degrees per second.
		 * @param left_reverse  Whether the left motor should go backward.
		 * @param right_reverse Whether the right motor should go backward.
		 */
		void push_speed(int value, bool left_reverse, bool right_reverse);

		/**
		 * \brief Wait until the motors stop.
		 */
		void wait_move() const;

		large_motor *left;          ///< pointer to the left motor
		large_motor *right;         ///< pointer to the right motor
		touch_sensor *touch;        ///< pointer to the touch sensor, might stay NULL
		gyro_sensor *gyro;          ///< pointer to the gyro sensor, might stay NULL
		int speed_fwd = 360;        ///< wheels speed during straight moves in degrees per second
		int speed_turn = 360;       ///< wheels speed during rotations in degrees per second
		const float turn_rate;      ///< ratio for calculating rotations, track width over wheel diameter
		const float forward_rate;   ///< wheel perimeter
		const float track_width;    ///< track width (for arcs)
		float turn_step_kp = 1.0f;  ///< proportional constant for step-corrected turning
		pid turn_linear_pid;        ///< pid regulator for continuous-correction turning
		static const unsigned int POLL_USEC = 2500;  ///< polling delta time
	};
}

#endif //ROBOFAT_STEP_PILOT_H
