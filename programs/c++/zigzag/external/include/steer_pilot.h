/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Differential motor controller.
 */

#ifndef ROBOFAT_STEER_PILOT_H
#define ROBOFAT_STEER_PILOT_H

#include "ev3dev.h"

#ifdef HAS_CURSES

#include "display.h"

#endif

using ev3dev::large_motor;
using ev3dev::address_type;

namespace robofat {
	/**
	 * \brief Differential motor controller accepting speed difference between wheels
	 *
	 * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
	 */
	class steer_pilot {
	public:

#ifdef HAS_CURSES

		/**
		 * \brief Create new controller instance.
		 *
		 * @param left              Address of left motor.
		 * @param right             Address of right motor.
		 * @param scr               Display for the question for user. The user won't be asked for action when motors aren't connected if this is nullptr.
		 * @throws device_not_found If user cannot be prompted for action and the motors aren't connected.
		 */
		steer_pilot(const address_type &left, const address_type &right, display *scr = nullptr);

#else
		/**
		 * \brief Create new controller instance.
		 *
		 * @param left              Address of left motor.
		 * @param right             Address of right motor.
		 * @throws device_not_found When the motors aren't connected.
		 */
		steer_pilot(const address_type &left, const address_type &right);
#endif

		/**
		 * \brief Destructs this controller.
		 */
		~steer_pilot();

		/**
		 * \brief Set new motor speed according to new difference between wheels speed.
		 *
		 * @param ccw   How much the robot should turn counter-clockwise.
		 */
		void push(float ccw);

		/**
		 * \brief Stop both motors.
		 */
		void stop();

		/**
		 * \brief Check if the left wheel is inverted.
		 *
		 * @return   {@code true} if the wheel is inverted, {@code false} otherwise
		 */
		bool get_left_invert();

		/**
		 * \brief Check if the right wheel is inverted.
		 *
		 * @return   {@code true} if the wheel is inverted, {@code false} otherwise
		 */
		bool get_right_invert();

		/**
		 * \brief Set whether the left wheel is inverted.
		 *
		 * @param invert   {@code true} if the wheel is to become inverted, {@code false} otherwise
		 */
		void set_left_invert(bool invert);

		/**
		 * \brief Set whether the right wheel is inverted.
		 *
		 * @param invert   {@code true} if the wheel is to become inverted, {@code false} otherwise
		 */
		void set_right_invert(bool invert);

		/**
		 * \brief Get middle robot speed.
		 *
		 * @return   Speed in units per second.
		 */
		int get_base_speed();

		/**
		 * \brief Set middle robot speed.
		 *
		 * @param speed   Speed in units per second.
		 */
		void set_base_speed(int speed);

		/**
		 * \brief Get max wheel speed.
		 *
		 * @return   Speed in units per second.
		 */
		int get_max_speed();

		/**
		 * \brief Set max wheel speed.
		 *
		 * @param speed   Speed in units per second.
		 */
		void set_max_speed(int speed);

		/**
		 * \brief Check if the speed clipped away from faster wheel is subtracted from the slower wheel's speed.
		 *
		 * @return   {@code true} if clip is carried, {@code false} otherwise
		 */
		bool get_clip_overflow();

		/**
		 * \brief Set whether the speed clipped away from faster wheel is subtracted from the slower wheel's speed.
		 *
		 * @param enabled   {@code true} if clip is to be carried, {@code false} otherwise
		 */
		void enable_clip_overflow(bool enabled);

		/**
		 * \brief Get ramp up time (i.e. after what time is the maximum power applied)
		 *
		 * @return   Ramp up time in milliseconds.
		 */
		int get_ramp_up();

		/**
		 * \brief Get ramp down time  (i.e. after what time is the minimum power applied)
		 *
		 * @return   Ramp down time in milliseconds.
		 */
		int get_ramp_down();

		/**
		 * \brief Set ramp up time (i.e. after what time is the maximum power applied)
		 *
		 * @param msec   Ramp up time in milliseconds.
		 */
		void set_ramp_up(int msec);

		/**
		 * \brief Set ramp down time (i.e. after what time is the maximum power applied)
		 *
		 * @param msec   Ramp down time in milliseconds.
		 */
		void set_ramp_down(int msec);

	private:
		/**
		 * \brief Set speed on specified motor.
		 *
		 * @param motor   Motor to set the speed on.
		 * @param speed   Speed to set.
		 */
		void push_power(ev3dev::large_motor *motor, int speed);


		large_motor *leftMotor = nullptr;  ///< Left motor
		large_motor *rightMotor = nullptr; ///< Right motor
		bool leftInvert = false;  ///< Whether the left wheel is rotated by 180°
		bool rightInvert = false; ///< Whether the right wheel is rotated by 180°
		int defaultSpeed = 360;   ///< Base speed for adjustments
		int maxSpeed = 720;       ///< Maximum speed for clipping
		bool clipFlow = false;    ///< Whether the overflow of the speed should be subtracted from the second wheel
	};
}

#endif //ROBOFAT_STEER_PILOT_H
