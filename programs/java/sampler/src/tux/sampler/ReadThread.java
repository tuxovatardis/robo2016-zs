package tux.sampler;

import lejos.hardware.Button;
import lejos.utility.Delay;
import tux.shared.AdvancedRunnable;
import tux.shared.led.Change;
import tux.shared.led.Color;
import tux.shared.led.LED;
import tux.shared.sensor.HackedSensor;

import java.io.PrintWriter;

/**
 * Sensor reader thread
 */
class ReadThread implements AdvancedRunnable {
    private static final long SAMPLE_TIME = 1000 * 1000;
    private final HackedSensor s;
    private final PrintWriter out;
    private volatile boolean run = true;
    private Thread thread = null;

    ReadThread(HackedSensor s, PrintWriter out) {
        this.out = out;
        this.s = s;
    }

    @Override
    public Thread getThread() {
        if (thread == null) {
            thread = new Thread(this);
            thread.setPriority(Thread.NORM_PRIORITY + 1);
            thread.setDaemon(false);
            thread.setName("reader");
        }
        return thread;
    }

    public void setStopFlag(boolean value) {
        run = !value;
    }

    @Override
    public void run() {
        Button.LEDPattern(LED.getPattern(Color.GREEN, Change.STEADY));
        long oldTimeStamp = System.nanoTime();
        while (run) {
            // measure the value
            long measureTimestamp = System.nanoTime();
            int value = s.getRedFast();
            out.print(measureTimestamp);
            out.print(',');
            out.println(value);
            System.out.println(value);
            // wait up to one millisecond
            long newTimeStamp = System.nanoTime();
            long diff = newTimeStamp - oldTimeStamp;
            oldTimeStamp = newTimeStamp;
            if (diff > SAMPLE_TIME)
                Delay.nsDelay(SAMPLE_TIME - diff);
        }
    }
}
