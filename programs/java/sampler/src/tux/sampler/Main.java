package tux.sampler;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Keys;
import lejos.hardware.port.Port;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) throws IOException {
        Settings.load(Settings.CONFIG);
        Brick brick = BrickFinder.getLocal();
        Keys keys = brick.getKeys();

        Port sensorPort = brick.getPort(Settings.SENSOR_PORT);
        HackedSensor sensor = new HackedSensor(sensorPort);

        try (FileWriter stream = new FileWriter("/home/lejos/programs/datalog.csv", false);
             BufferedWriter buffer = new BufferedWriter(stream, 1024 * 1024);
             PrintWriter writer = new PrintWriter(buffer, false)) {
            ReadThread reader = new ReadThread(sensor, writer);
            reader.getThread().start();
            //noinspection StatementWithEmptyBody
            while ((keys.waitForAnyPress() & Keys.ID_ESCAPE) == 0) {
            }
            reader.setStopFlag(true);
            stream.flush();
        }
    }
}
