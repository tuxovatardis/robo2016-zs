package tux.alter;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Keys;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import tux.alter.mechanics.PID2Mover;
import tux.shared.TrackMode;
import tux.shared.UserMain;
import tux.shared.led.Change;
import tux.shared.led.Color;
import tux.shared.led.LED;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;

/**
 * Alternative PID regulator based line follower
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.7
 */
public class Main implements Runnable {
    /**
     * Current line tracking mode
     */
    public static TrackMode TRACK_MODE;
    private final UserMain user;
    private final Brick brick;
    private final lejos.hardware.LED led;
    private PID2Mover mover;
    //private SensorHolder hold;

    private Main() {
        this.brick = BrickFinder.getLocal();
        led = brick.getLED();
        LED.setPattern(led, Color.RED, Change.BLINK);
        Keys keys = brick.getKeys();
        TextLCD lcd = brick.getTextLCD(Font.getDefaultFont());
        user = new UserMain(lcd, keys, led, "- JVanek -");
    }

    public static void main(String[] args) {
        new Main().run();
    }

    @Override
    public void run() {
        TRACK_MODE = user.getTrackMode();
        if (TRACK_MODE == null)
            return;
        user.loadSettings();
        load();
        if (!user.getStart())
            return;
        start();
        waitForEnd();
    }

    private void start() {
        mover.getThread().start();
    }

    private void waitForEnd() {
        user.waitForEnd();
        mover.setStopFlag(true);
        //user.tryClose(mover.getThread(), );
        LED.turnOff(led);
    }

    /**
     * Load everything that interacts with outside world by physical way i.e. color sensor, motors, chassis
     */
    private void load() {
        // get ports
        //final Port sensorMotorPort = brick.getPort(Settings.SENSOR_MOTOR_PORT);
        final Port sensorPort = brick.getPort(Settings.SENSOR_PORT);
        final Port motorLeftPort = brick.getPort(Settings.WHEEL_LEFT_PORT);
        final Port motorRightPort = brick.getPort(Settings.WHEEL_RIGHT_PORT);
        final Port motorSensorPort = brick.getPort(Settings.SENSOR_MOTOR_PORT);
        // load motors
        final RegulatedMotor motorLeft = user.loadLargeMotor(motorLeftPort);
        final RegulatedMotor motorRight = user.loadLargeMotor(motorRightPort);
        final UnregulatedMotor motorSensor = user.loadUnregulatedMotor(motorSensorPort);
        // load color sensor
        final HackedSensor sensor = user.loadSensor(sensorPort);
        // create
        //hold = new SensorHolder(sensorMotorPort);
        mover = new PID2Mover(motorLeft, motorRight, motorSensor, sensor);

        // place sensor to the right
        LED.setPattern(led, Color.RED, Change.BLINK);
        mover.placeSensor();
        LED.setPattern(led, Color.GREEN, Change.STEADY);
    }
}
