package tux.zigzag.sensor;

import tux.shared.AdvancedRunnable;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Class for separating data from individual swipes
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class DataPacker implements AdvancedRunnable, Closeable {
    /**
     * Initial capacity of sample list
     */
    private static final int START_CAPACITY = 300;
    /**
     * Warm-up counter value for the first stage, where the packer fetches the first sample.
     */
    private static final int WARMUP_GETANGLE = 0;
    /**
     * Warm-up counter value for the second stage, where the packer determines the sensor's direction.
     */
    private static final int WARMUP_GETTREND = 1;
    /**
     * Warm-up counter value for the third stage, when it has all the information for pushing new packs.
     */
    private static final int WARMUP_DONE = 2;
    /**
     * Sensor reader for getting samples
     */
    private final SensorReader reader;
    /**
     * Output queue
     */
    private final BlockingQueue<Pack> output;
    /**
     * Thread instance for {@link #getThread()}
     */
    private Thread threadInstance = null;
    /**
     * Run flag
     */
    private volatile boolean run = true;

    /**
     * Constructs new sensor reader.
     *
     * @param output Output queue
     */
    public DataPacker(SensorReader sensor, BlockingQueue<Pack> output) {
        this.reader = Objects.requireNonNull(sensor, "Sensor must not be null!");
        this.output = Objects.requireNonNull(output, "Output must not be null!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Thread getThread() {
        if (threadInstance == null) {
            threadInstance = new Thread(this);
            threadInstance.setPriority(Thread.NORM_PRIORITY);
            threadInstance.setDaemon(true);
            threadInstance.setName("packer");
        }
        return threadInstance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStopFlag(boolean flag) {
        run = !flag;
        if (threadInstance != null && flag)
            threadInstance.interrupt();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        // sample buffer
        List<Sample> list = new ArrayList<>(START_CAPACITY);
        // last angle
        int lastAngle = Integer.MIN_VALUE;
        // if the value is increasing
        boolean increase = false;
        // startup initialization sequence counter
        int warmup_counter = WARMUP_GETANGLE;
        // run
        Sample sample = null;
        while (run) {
            // sleep until new data are available
            if (!reader.sleep(sample)) continue;
            // fetch sample
            sample = reader.fetchSample();
            if (sample == null)
                continue;
            int angle = sample.getAngle();
            // if initialization is complete
            if (warmup_counter == WARMUP_DONE) {
                // if there's inverse trend of the value increase
                if (angle > lastAngle && !increase || angle < lastAngle && increase) {
                    // determine direction
                    Direction dir = increase ? Direction.RIGHT_TO_LEFT : Direction.LEFT_TO_RIGHT;
                    // toggle trend
                    increase = !increase;
                    // push list
                    try {
                        output.offer(new Pack(list, dir), 10, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // create new list
                    list = new ArrayList<>(START_CAPACITY);
                }
                // we have fetched some angles, but trend was not determined yet
            } else if (warmup_counter == WARMUP_GETTREND) {
                // check for trend, update and advance to the next init level accordingly
                if (angle < lastAngle) {
                    increase = false;
                    warmup_counter++;
                } else if (angle > lastAngle) {
                    increase = true;
                    warmup_counter++;
                }
                // first spin, get first angle (moved to the end), advance to the next init level
            } else if (warmup_counter == WARMUP_GETANGLE) {
                warmup_counter++;
            }
            // save angle value
            lastAngle = angle;
            // push sample
            list.add(sample);
        }
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
