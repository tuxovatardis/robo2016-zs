package tux.zigzag.sensor;

import java.util.List;
import java.util.Objects;

/**
 * Pack containing list of samples and common metadata
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.1
 */
public class Pack {
    /**
     * List of samples
     */
    private final List<Sample> samples;
    /**
     * Direction of sampling
     */
    private final Direction direction;

    /**
     * Constructs new packet.
     *
     * @param samples   Samples, must not be null
     * @param direction Sampling direction
     */
    public Pack(List<Sample> samples, Direction direction) {
        this.samples = Objects.requireNonNull(samples);
        this.direction = direction;
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public Direction getDirection() {
        return direction;
    }

}
