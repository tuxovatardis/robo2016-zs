/**
 * Package containing color sensor related classes.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 * @since 0.0
 */
package tux.zigzag.sensor;