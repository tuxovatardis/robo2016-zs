package tux.zigzag;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Keys;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.chassis.Chassis;
import tux.shared.TrackMode;
import tux.shared.UserMain;
import tux.shared.led.Change;
import tux.shared.led.Color;
import tux.shared.led.LED;
import tux.shared.mechanics.Mechanics;
import tux.shared.sensor.HackedSensor;
import tux.shared.sensor.SensorRotor;
import tux.shared.utils.Settings;
import tux.zigzag.mechanics.ThresholdMover;
import tux.zigzag.sensor.DataPacker;
import tux.zigzag.sensor.Pack;
import tux.zigzag.sensor.SensorReader;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Main program
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class Main implements Runnable {
    /**
     * Inter-thread queue max capacity
     */
    private static final int QUEUE_CAPACITY = 2;
    /**
     * Current line tracking mode
     */
    public static TrackMode TRACK_MODE;
    /**
     * User interface
     */
    private final UserMain user;
    /**
     * LED used for indication
     */
    private final lejos.hardware.LED led;
    /**
     * Brick instance
     */
    private final Brick brick;
    /**
     * Sensor rotor
     */
    private SensorRotor rotor = null;
    /**
     * Data packer
     */
    private DataPacker pack = null;
    /**
     * Effector
     */
    private ThresholdMover reactor = null;
    private RegulatedMotor motorLeft;
    private RegulatedMotor motorRight;

    /**
     * Constructs new instance of main program
     */
    private Main() {
        // get all the required things
        brick = BrickFinder.getLocal();
        led = brick.getLED(); // indicate that program has started
        LED.setPattern(led, Color.RED, Change.BLINK);
        Keys keys = brick.getKeys();
        TextLCD lcd = brick.getTextLCD(Font.getDefaultFont());
        user = new UserMain(lcd, keys, led, "- zigzag -");
    }

    /**
     * Main entry point
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        new Main().run();
    }

    /**
     * Load everything that interacts with outside world by physical way i.e. color sensor, motors, chassis
     */
    private void loadEnvironment() {
        // get ports
        final Port sensorMotorPort = brick.getPort(Settings.SENSOR_MOTOR_PORT);
        final Port sensorPort = brick.getPort(Settings.SENSOR_PORT);
        final Port motorLeftPort = brick.getPort(Settings.WHEEL_LEFT_PORT);
        final Port motorRightPort = brick.getPort(Settings.WHEEL_RIGHT_PORT);
        // load motors
        motorLeft = user.loadLargeMotor(motorLeftPort);
        motorRight = user.loadLargeMotor(motorRightPort);
        final Chassis ch = Mechanics.createChassis(motorLeft, motorRight);
        //final ExperimentalTARDIS tardis = Mechanics.createTARDIS(motorLeft, motorRight);
        final UnregulatedMotor sensorMotor = new UnregulatedMotor(sensorMotorPort);
        // load color sensor
        final HackedSensor sensor = user.loadSensor(sensorPort);
        // create queues
        final BlockingQueue<Pack> pack_effect = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
        // create threads
        // Sensor reader
        SensorReader reader = new SensorReader(sensor, sensorMotor);
        rotor = new SensorRotor(sensorMotor);
        pack = new DataPacker(reader, pack_effect);
        //reactor = new ThresholdMover(tardis, pack_effect, rotor);
        reactor = new ThresholdMover(ch, pack_effect, rotor);
        // place sensor to the right
        LED.setPattern(led, Color.RED, Change.BLINK);
        rotor.placeSensor();
        LED.setPattern(led, Color.GREEN, Change.STEADY);
    }

    /**
     * Start the zigzag's odyssey
     */
    private void start() {
        // start in this order: sensor reader/packer, sensor mover, effector
        pack.getThread().start();
        rotor.getThread().start();
        reactor.getThread().start();
    }

    /**
     * Wait for exit
     */
    private void waitForEnd() {
        user.waitForEnd();
        pack.setStopFlag(true);
        reactor.setStopFlag(true);
        rotor.setStopFlag(true);
        user.tryClose(reactor.getThread(), motorLeft, motorRight);
        user.tryClose(pack.getThread(), pack);
        user.tryClose(rotor.getThread(), rotor);
        LED.turnOff(led);
    }

    @Override
    public void run() {
        TRACK_MODE = user.getTrackMode();
        if (TRACK_MODE == null)
            return;
        user.loadSettings();
        loadEnvironment();
        if (!user.getStart())
            return;
        start();
        waitForEnd();
    }
}
