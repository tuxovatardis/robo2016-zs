/**
 * Shared classes for the programs of the team "Tuxova TARDIS" for Robosoutěž 2016 pro ZŠ
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.4
 */
package tux.shared;