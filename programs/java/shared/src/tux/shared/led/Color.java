package tux.shared.led;

/**
 * Color of the light
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public enum Color {
    /**
     * LEDs are off
     */
    OFF(0),
    /**
     * LEDs emit green light
     */
    GREEN(1),
    /**
     * LEDs emit red light
     */
    RED(2),
    /**
     * LEDs emit both green and red light, creating orange/amber color.
     */
    ORANGE(3);
    private final int code;

    Color(int code) {
        this.code = code;
    }

    /**
     * Get code associated with this color.
     *
     * @return Code, NOT additive
     */
    public int getCode() {
        return code;
    }
}
