package tux.shared.led;

/**
 * LED patterns for LEGO kernel module
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public final class LED {
    private LED() {
    }

    /**
     * Get numeric representation of a LED pattern
     *
     * @param color  Color of the light
     * @param change Changes over time
     * @return Argument to {@link lejos.hardware.LED#setPattern(int)}
     */
    public static int getPattern(Color color, Change change) {
        if (color == Color.OFF)
            return Color.OFF.getCode();
        return color.getCode() + change.getCode();
    }

    /**
     * Set LED pattern
     *
     * @param led    LED to set the pattern on
     * @param color  Color of the light
     * @param change Changes over time
     */
    public static void setPattern(lejos.hardware.LED led, Color color, Change change) {
        led.setPattern(getPattern(color, change));
    }

    /**
     * Turn off LED
     *
     * @param led The LED to turn off
     */
    public static void turnOff(lejos.hardware.LED led) {
        led.setPattern(Color.OFF.getCode());
    }

}
