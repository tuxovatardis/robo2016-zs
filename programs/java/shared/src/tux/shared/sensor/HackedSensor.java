package tux.shared.sensor;

import lejos.hardware.port.Port;
import lejos.hardware.port.UARTPort;

import java.io.Closeable;
import java.io.IOException;

/**
 * Hacked EV3 color sensor
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class HackedSensor implements Closeable {
    /**
     * Code for the "COL-REFLECT" sensor mode.
     */
    private static final int COL_REFLECT = 0;
    /**
     * UART port instance.
     */
    private final UARTPort port;

    /**
     * Constructs new instance of hacked color sensor<br/>
     * See {@link lejos.hardware.sensor.EV3ColorSensor#EV3ColorSensor(Port)}
     *
     * @param port Port where the sensor is attached
     */
    public HackedSensor(Port port) {
        this.port = port.open(UARTPort.class);
        if (!this.port.setMode(COL_REFLECT)) {
            this.port.close();
            throw new IllegalArgumentException("Invalid sensor mode");
        }
    }

    /**
     * Directly fetch sensor value from the port
     *
     * @return Value in range 0-100
     */
    public byte getRedFast() {
        return port.getByte();
    }

    @Override
    public void close() throws IOException {
        this.port.close();
    }
}
