/**
 * Classes for manipulating the color sensor.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.4
 */
package tux.shared.sensor;