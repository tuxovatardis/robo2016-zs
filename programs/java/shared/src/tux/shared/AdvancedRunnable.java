package tux.shared;

/**
 * Runnable extended with additional helper functions.
 * <p/>
 * {@inheritDoc}
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.1
 * @since 0.2
 */
public interface AdvancedRunnable extends Runnable {
    /**
     * Construct a new thread from this runnable. Multiple invocations on the same object must return same references.<br/>
     * Note: Subclasses are not required to be able to execute multiple times.<br/>
     *
     * @return Thread for this runnable
     */
    Thread getThread();

    /**
     * Set the stop flag for this runnable.
     * Note: this is designed to be invoked from another thread but the thread executing run().
     *
     * @param flag Whether the runnable should stop.
     */
    void setStopFlag(boolean flag);
}
