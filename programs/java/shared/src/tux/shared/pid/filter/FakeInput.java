package tux.shared.pid.filter;

import lejos.robotics.SampleProvider;

/**
 * Created by kuba on 5.4.16.
 */
public class FakeInput implements SampleProvider {
    private float value;

    public FakeInput(float val) {
        value = val;
    }

    @Override
    public int sampleSize() {
        return 1;
    }

    @Override
    public void fetchSample(float[] sample, int offset) {
        sample[offset] = value;
    }

    public void setValue(float val) {
        value = val;
    }

}
