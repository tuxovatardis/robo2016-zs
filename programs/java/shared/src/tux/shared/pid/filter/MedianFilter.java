package tux.shared.pid.filter;

/**
 * Created by kuba on 5.4.16.
 */
public class MedianFilter implements Filter {
    private FakeInput fakeInput = new FakeInput(0f);
    private lejos.robotics.filter.MedianFilter filter;

    public MedianFilter(int size) {
        filter = new lejos.robotics.filter.MedianFilter(fakeInput, size);
    }


    @Override
    public float filter(float input) {
        fakeInput.setValue(input);
        float[] data = new float[1];
        filter.fetchSample(data, 0);
        return data[0];
    }
}
