package tux.shared.pid;

import lejos.robotics.RegulatedMotor;
import tux.shared.utils.Utils;

/**
 * Differential pilot controller, input is speed difference between wheels
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.7
 */
public class DifferentialController {
    private RegulatedMotor left;
    private RegulatedMotor right;
    private boolean leftInverted;
    private boolean rightInverted;
    private int defaultSpeed;
    private int maxSpeed;
    private int slowdownLimit;
    private float slowdownCoef;
    private int lastLeft;
    private int lastRight;

    public DifferentialController(RegulatedMotor left, RegulatedMotor right, boolean leftInvert, boolean rightInvert) {
        this.left = left;
        this.right = right;
        left.setStallThreshold(360, 1000);
        right.setStallThreshold(360, 1000);
        this.leftInverted = leftInvert;
        this.rightInverted = rightInvert;
        defaultSpeed = 360;
        this.maxSpeed = Math.round(Math.min(left.getMaxSpeed(), right.getMaxSpeed()));
        this.slowdownLimit = Integer.MAX_VALUE;
        this.slowdownCoef = 1f;
    }

    public int getLastLeft() {
        return lastLeft;
    }

    public int getLastRight() {
        return lastRight;
    }

    public int getDefaultSpeed() {
        return defaultSpeed;
    }

    public void setDefaultSpeed(int defaultSpeed) {
        this.defaultSpeed = defaultSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getSlowdownLimit() {
        return slowdownLimit;
    }

    public void setSlowdownLimit(int slowdownLimit) {
        this.slowdownLimit = slowdownLimit;
    }

    public float getSlowdownCoef() {
        return slowdownCoef;
    }

    public void setSlowdownCoef(float slowdownCoef) {
        this.slowdownCoef = slowdownCoef;
    }

    private void pushPower(RegulatedMotor motor, int speed, boolean backward, boolean inverted) {
        /*
        speed += defaultSpeed;
        final boolean backward = speed < 0;
        speed = Math.abs(speed);

        if (Math.abs(error) > slowdownLimit)
            speed -= (Math.abs(error) - slowdownLimit) * slowdownCoef;
        speed = Utils.limit(speed, 0, maxSpeed);
*/
        motor.setSpeed(speed);

        if (inverted ^ backward)
            motor.backward();
        else
            motor.forward();
    }

    public void push(int ccw, int error) {
        error = Math.abs(error);
        int leftSpeed = defaultSpeed - ccw;
        int rightSpeed = defaultSpeed + ccw;
        /*
        if (ccw > 0) {
            leftSpeed -= ccw;
        } else if (ccw < 0) {
            rightSpeed += ccw;
        }*/
        final boolean leftBackward = leftSpeed < 0;
        final boolean rightBackward = rightSpeed < 0;
        leftSpeed = Math.abs(leftSpeed);
        rightSpeed = Math.abs(rightSpeed);

        if (error > slowdownLimit) {
            float ratio = (float) leftSpeed / (float) rightSpeed;
            float errorCorrection = (error - slowdownLimit) * slowdownCoef;
            if (leftSpeed > rightSpeed) {
                leftSpeed -= errorCorrection;
                rightSpeed = Math.round(leftSpeed / ratio);
            } else if (leftSpeed < rightSpeed) {
                rightSpeed -= errorCorrection;
                leftSpeed = Math.round(rightSpeed * ratio);
            } else {
                leftSpeed -= errorCorrection;
                rightSpeed -= errorCorrection;
            }
        }
        leftSpeed = Utils.limit(leftSpeed, 0, maxSpeed);
        rightSpeed = Utils.limit(rightSpeed, 0, maxSpeed);

        left.startSynchronization();
        pushPower(left, lastLeft = leftSpeed, leftBackward, leftInverted);
        pushPower(right, lastRight = rightSpeed, rightBackward, rightInverted);
        left.endSynchronization();
    }


    public void setAccel(int accel) {
        left.startSynchronization();
        left.setAcceleration(accel);
        right.setAcceleration(accel);
        left.endSynchronization();
    }

    public void stop() {
        left.startSynchronization();
        left.stop(true);
        right.stop(true);
        left.endSynchronization();
    }
}
