package tux.shared.utils;

import java.util.concurrent.BlockingQueue;

/**
 * Utility functions
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public final class Utils {
    private Utils() {
    }

    /**
     * {@link BlockingQueue#take()} element from the queue until nonnull element is obtained
     *
     * @param queue Blocking queue to take from
     * @param <T>   Type of element
     * @return Element or {@code null} if take was interrupted
     */
    public static <T> T take(BlockingQueue<T> queue) {
        T s;
        do {
            try {
                s = queue.take();
            } catch (InterruptedException e) {
                return null;
            }
        } while (s == null);
        return s;
    }

    public static int limit(int number, int min, int max) {
        number = Math.max(number, min);
        return Math.min(number, max);
    }
}
