/**
 * General and wide-usable classes and methods
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
package tux.shared.utils;