package tux.shared.utils;

import java.io.*;

/**
 * (Hopefully) fast settings storage
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.1
 */
public final class Settings {
    /**
     * Main config path
     */
    public static final String CONFIG = "/home/lejos/programs/tux.conf";
    private static final int SIZE_OFFSET = 'a' - 'A';
    public static boolean WHEEL_LEFT_INVERT = false;
    public static boolean WHEEL_RIGHT_INVERT = false;
    public static String WHEEL_LEFT_PORT = "B";
    public static String WHEEL_RIGHT_PORT = "C";
    public static String SENSOR_MOTOR_PORT = "A";
    public static String SENSOR_PORT = "S1";
    public static int SENSOR_CENTER_OFFSET = 35;
    // PID import
    public static int PID_SENSOR_TARGET = 50;

    //
    // PID2 alias The Alternative
    //
    public static float PID2_SENSOR_KP;
    public static float PID2_SENSOR_KI;
    public static float PID2_SENSOR_KD;
    public static float PID2_WHEELS_KP;
    public static float PID2_WHEELS_KI;
    public static float PID2_WHEELS_KD;

    public static String PID2_SENSOR_FILTER_TYPE;
    public static String PID2_WHEELS_FILTER_TYPE;
    public static String PID2_TRANS_FILTER_TYPE;

    public static float PID2_SENSOR_FILTER_ARG1;
    public static float PID2_SENSOR_FILTER_ARG2;
    public static float PID2_WHEELS_FILTER_ARG1;
    public static float PID2_WHEELS_FILTER_ARG2;
    public static float PID2_TRANS_FILTER_ARG1;
    public static float PID2_TRANS_FILTER_ARG2;

    public static int PID2_WHEELS_SPEED;
    public static int PID2_WHEELS_ACCEL;
    public static int PID2_SENSOR_SPEED;
    public static int PID2_SENSOR_ACCEL;
    public static int PID2_SENSOR_MIN;
    public static int PID2_SENSOR_MAX;
    public static int PID2_SLEEP;
    public static float PID2_OVERFLOW_COEF;
    public static int PID2_WHEELS_SLOWDOWN_LIMIT;
    public static float PID2_WHEELS_SLOWDOWN_KP;

    public static String PID2_SLOWDOWN_FILTER_TYPE;
    public static float PID2_SLOWDOWN_FILTER_ARG2;
    public static float PID2_SLOWDOWN_FILTER_ARG1;

    public static float PID2_TARGET_COEF;
    public static int PID2_LIGHT_LIMIT;
    public static float PID2_LIGHT_COEF_BLACK;
    public static float PID2_LIGHT_COEF_WHITE;


    private Settings() {
    }

    public static void load(String name) throws IOException {
        load(new File(name));
    }

    public static void save(String name) throws IOException {
        save(new File(name));
    }

    public static void load(File f) throws IOException {
        try (FileReader stream = new FileReader(f);
             BufferedReader read = new BufferedReader(stream)) {
            String line;
            while ((line = read.readLine()) != null) {
                if (line.trim().length() == 0)
                    continue;
                if (line.charAt(0) == '#')
                    continue;
                int ch = line.indexOf('=');
                String before = line.substring(0, ch).trim();
                String after = line.substring(ch + 1).trim();
                // TODO implement parser
                switch (toLower(before)) {
                    //
                    // ROBOT PROPERTIES
                    //
                    case "left wheel inverted":
                        WHEEL_LEFT_INVERT = Boolean.parseBoolean(after);
                        break;
                    case "right wheel inverted":
                        WHEEL_RIGHT_INVERT = Boolean.parseBoolean(after);
                        break;
                    case "effect sensor offset":
                        SENSOR_CENTER_OFFSET = Integer.parseInt(after);
                        break;
                    //
                    // PORTS
                    //
                    case "left motor port":
                        WHEEL_LEFT_PORT = toUpper(after);
                        break;
                    case "right motor port":
                        WHEEL_RIGHT_PORT = toUpper(after);
                        break;
                    case "sensor motor port":
                        SENSOR_MOTOR_PORT = toUpper(after);
                        break;
                    case "sensor port":
                        SENSOR_PORT = toUpper(after);
                        break;
                    //
                    // PID2 alias The Alternative
                    //
                    case "pid2 sensor target":
                        PID_SENSOR_TARGET = Integer.parseInt(after);
                        break;
                    case "pid2 sensor kp":
                        PID2_SENSOR_KP = Float.parseFloat(after);
                        break;
                    case "pid2 sensor ki":
                        PID2_SENSOR_KI = Float.parseFloat(after);
                        break;
                    case "pid2 sensor kd":
                        PID2_SENSOR_KD = Float.parseFloat(after);
                        break;
                    case "pid2 wheels kp":
                        PID2_WHEELS_KP = Float.parseFloat(after);
                        break;
                    case "pid2 wheels ki":
                        PID2_WHEELS_KI = Float.parseFloat(after);
                        break;
                    case "pid2 wheels kd":
                        PID2_WHEELS_KD = Float.parseFloat(after);
                        break;

                    case "pid2 sensor filter type":
                        PID2_SENSOR_FILTER_TYPE = after;
                        break;
                    case "pid2 sensor filter arg1":
                        PID2_SENSOR_FILTER_ARG1 = Float.parseFloat(after);
                        break;
                    case "pid2 sensor filter arg2":
                        PID2_SENSOR_FILTER_ARG2 = Float.parseFloat(after);
                        break;
                    case "pid2 wheels filter type":
                        PID2_WHEELS_FILTER_TYPE = after;
                        break;
                    case "pid2 wheels filter arg1":
                        PID2_WHEELS_FILTER_ARG1 = Float.parseFloat(after);
                        break;
                    case "pid2 wheels filter arg2":
                        PID2_WHEELS_FILTER_ARG2 = Float.parseFloat(after);
                        break;
                    case "pid2 trans filter type":
                        PID2_TRANS_FILTER_TYPE = after;
                        break;
                    case "pid2 trans filter arg1":
                        PID2_TRANS_FILTER_ARG1 = Float.parseFloat(after);
                        break;
                    case "pid2 trans filter arg2":
                        PID2_TRANS_FILTER_ARG2 = Float.parseFloat(after);
                        break;
                    case "pid2 slowdown filter type":
                        PID2_SLOWDOWN_FILTER_TYPE = after;
                        break;
                    case "pid2 slowdown filter arg1":
                        PID2_SLOWDOWN_FILTER_ARG1 = Float.parseFloat(after);
                        break;
                    case "pid2 slowdown filter arg2":
                        PID2_SLOWDOWN_FILTER_ARG2 = Float.parseFloat(after);
                        break;

                    case "pid2 wheels speed":
                        PID2_WHEELS_SPEED = Integer.parseInt(after);
                        break;
                    case "pid2 wheels accel":
                        PID2_WHEELS_ACCEL = Integer.parseInt(after);
                        break;
                    case "pid2 sensor speed":
                        PID2_SENSOR_SPEED = Integer.parseInt(after);
                        break;
                    case "pid2 sensor accel":
                        PID2_SENSOR_ACCEL = Integer.parseInt(after);
                        break;
                    case "pid2 sensor min":
                        PID2_SENSOR_MIN = Integer.parseInt(after);
                        break;
                    case "pid2 sensor max":
                        PID2_SENSOR_MAX = Integer.parseInt(after);
                        break;
                    case "pid2 sleep":
                        PID2_SLEEP = Integer.parseInt(after);
                        break;
                    case "pid2 overflow coef":
                        PID2_OVERFLOW_COEF = Float.parseFloat(after);
                        break;
                    case "pid2 wheels slowdown limit":
                        PID2_WHEELS_SLOWDOWN_LIMIT = Integer.parseInt(after);
                        break;
                    case "pid2 wheels slowdown kp":
                        PID2_WHEELS_SLOWDOWN_KP = Float.parseFloat(after);
                        break;
                    case "pid2 target coef":
                        PID2_TARGET_COEF = Float.parseFloat(after);
                        break;
                    case "pid2 light limit":
                        PID2_LIGHT_LIMIT = Integer.parseInt(after);
                        break;
                    case "pid2 light coef black":
                        PID2_LIGHT_COEF_BLACK = Float.parseFloat(after);
                        break;
                    case "pid2 light coef white":
                        PID2_LIGHT_COEF_WHITE = Float.parseFloat(after);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public static void save(File f) throws IOException {
        try (FileWriter stream = new FileWriter(f, false);
             BufferedWriter buf = new BufferedWriter(stream);
             PrintWriter w = new PrintWriter(buf)) {
            w.println("### TARDIS Configuration File");
            w.println("# ~ Handle With Care ~");
            w.println();

            w.println("##");
            w.println("## PID2 alias The Alternative");
            w.println("##");
            w.println();
            w.println("# pids");
            w.println("pid2 sensor Kp = " + PID2_SENSOR_KP);
            w.println("pid2 sensor Ki = " + PID2_SENSOR_KI);
            w.println("pid2 sensor Kd = " + PID2_SENSOR_KD);
            w.println("pid2 wheels Kp = " + PID2_WHEELS_KP);
            w.println("pid2 wheels Ki = " + PID2_WHEELS_KI);
            w.println("pid2 wheels Kd = " + PID2_WHEELS_KD);
            w.println();
            w.println("# filters");
            w.println("pid2 sensor filter type = " + PID2_SENSOR_FILTER_TYPE);
            w.println("pid2 sensor filter arg1 = " + PID2_SENSOR_FILTER_ARG1);
            w.println("pid2 sensor filter arg2 = " + PID2_SENSOR_FILTER_ARG2);
            w.println("pid2 wheels filter type = " + PID2_WHEELS_FILTER_TYPE);
            w.println("pid2 wheels filter arg1 = " + PID2_WHEELS_FILTER_ARG1);
            w.println("pid2 wheels filter arg2 = " + PID2_WHEELS_FILTER_ARG2);
            w.println("pid2 trans filter type = " + PID2_TRANS_FILTER_TYPE);
            w.println("pid2 trans filter arg1 = " + PID2_TRANS_FILTER_ARG1);
            w.println("pid2 trans filter arg2 = " + PID2_TRANS_FILTER_ARG2);
            w.println("pid2 slowdown filter type = " + PID2_SLOWDOWN_FILTER_TYPE);
            w.println("pid2 slowdown filter arg1 = " + PID2_SLOWDOWN_FILTER_ARG1);
            w.println("pid2 slowdown filter arg2 = " + PID2_SLOWDOWN_FILTER_ARG2);
            w.println();
            w.println("# speeds & accelerations");
            w.println("pid2 wheels speed = " + PID2_WHEELS_SPEED);
            w.println("pid2 wheels accel = " + PID2_WHEELS_ACCEL);
            w.println("pid2 sensor speed = " + PID2_SENSOR_SPEED);
            w.println("pid2 sensor accel = " + PID2_SENSOR_ACCEL);
            w.println("# sensor properties");
            w.println("pid2 sensor min = " + PID2_SENSOR_MIN);
            w.println("pid2 sensor max = " + PID2_SENSOR_MAX);
            w.println("effect sensor offset = " + SENSOR_CENTER_OFFSET);
            w.println("pid2 sensor target = " + PID_SENSOR_TARGET);
            w.println();
            w.println("# sleep");
            w.println("pid2 sleep = " + PID2_SLEEP);
            w.println("# sensor limit exceedance");
            w.println("pid2 overflow coef = " + PID2_OVERFLOW_COEF);
            w.println("# slowdown");
            w.println("pid2 wheels slowdown limit = " + PID2_WHEELS_SLOWDOWN_LIMIT);
            w.println("pid2 wheels slowdown kp = " + PID2_WHEELS_SLOWDOWN_KP);
            w.println("# trial to save us");
            w.println("pid2 target coef = " + PID2_TARGET_COEF);
            w.println("pid2 light limit = " + PID2_LIGHT_LIMIT);
            w.println("pid2 light coef black = " + PID2_LIGHT_COEF_BLACK);
            w.println("pid2 light coef white = " + PID2_LIGHT_COEF_WHITE);
            w.println();
            w.println();

            w.println("## PORTS");
            w.println("left motor port = " + WHEEL_LEFT_PORT);
            w.println("right motor port = " + WHEEL_RIGHT_PORT);
            w.println("sensor motor port = " + SENSOR_MOTOR_PORT);
            w.println("sensor port = " + SENSOR_PORT);
            w.println();

            w.println("## WHEELS");
            w.println("left wheel inverted = " + WHEEL_LEFT_INVERT);
            w.println("right wheel inverted = " + WHEEL_RIGHT_INVERT);
            w.println();

            w.flush();
        }
    }

    private static String toLower(String str) {
        char[] arr = str.toCharArray();
        boolean change = false;
        for (int i = 0; i < arr.length; i++) {
            int ch = arr[i];
            if (ch >= 'A' && ch <= 'Z') {
                arr[i] = (char) (ch + SIZE_OFFSET);
                change = true;
            }
        }
        if (!change)
            return str;
        return new String(arr);
    }

    private static String toUpper(String str) {
        char[] arr = str.toCharArray();
        boolean change = false;
        for (int i = 0; i < arr.length; i++) {
            int ch = arr[i];
            if (ch >= 'a' && ch <= 'z') {
                arr[i] = (char) (ch - SIZE_OFFSET);
                change = true;
            }
        }
        if (!change)
            return str;
        return new String(arr);
    }

    private static String unquoteString(String str) {
        int start = 0;
        int end = str.length();
        if ((str.charAt(0) == '\"' && str.charAt(end - 1) == '\"') ||
                (str.charAt(0) == '\'' && str.charAt(end - 1) == '\''))
            return str.substring(start + 1, end - 1);
        return str;
    }
}
