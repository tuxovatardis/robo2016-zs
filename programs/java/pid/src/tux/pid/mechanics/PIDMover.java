package tux.pid.mechanics;

import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import tux.pid.Main;
import tux.shared.AdvancedRunnable;
import tux.shared.TrackMode;
import tux.shared.pid.DifferentialController;
import tux.shared.pid.PID;
import tux.shared.pid.filter.Filter;
import tux.shared.pid.filter.Filters;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;


/**
 * PID based mover
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.6
 */
public class PIDMover implements AdvancedRunnable {
    private final HackedSensor sensor;
    private Thread thread = null;
    private volatile boolean run = true;
    private PID pid;
    private DifferentialController control;

    public PIDMover(RegulatedMotor l, RegulatedMotor r, HackedSensor s) {
        l.synchronizeWith(new RegulatedMotor[]{r});
        this.sensor = s;
        float avgActual = Settings.PID1_AVGFILTER_ACTUAL;
        float avgHist = Settings.PID1_AVGFILTER_HISTORY;
        int medSize = Settings.PID1_MEDIANFILTER_SIZE;
        float lowPass = Settings.PID1_LOWPASSFILTER_CONSTANT;
        Filter derivationFilter = Filters.getNewFilter(Filters.getMode(Settings.PID1_FILTER_TYPE),
                avgActual, avgHist, medSize, lowPass);
        this.pid = new PID(Settings.PID1_KP,
                Settings.PID1_KI / Settings.PID1_TI,
                Settings.PID1_KD * Settings.PID1_TD,
                derivationFilter);
        this.control = new DifferentialController(l, r,
                Settings.WHEEL_LEFT_INVERT,
                Settings.WHEEL_RIGHT_INVERT);
        control.setDefaultSpeed(Settings.PID1_SPEED);
        control.setSlowdownLimit(Settings.PID1_SLOWDOWN_LIMIT);
        control.setSlowdownCoef(Settings.PID1_SLOWDOWN_KP);
        //logger = new Logger();
    }

    /**
     * Sleep to create more consistent timing
     *
     * @param lastTimestamp Last timestamp
     */
    private static long sleep(long lastTimestamp, int millis) {
        long stamp = System.nanoTime();
        long wait = stamp - lastTimestamp;
        Delay.msDelay(millis - Math.round(wait / 1_000_000f));
        return stamp;
    }

    @Override
    public Thread getThread() {
        if (thread == null) {
            thread = new Thread(this);
            thread.setPriority(Thread.MAX_PRIORITY);
            thread.setDaemon(false);
            thread.setName("mover");
        }
        return thread;
    }

    @Override
    public void setStopFlag(boolean flag) {
        this.run = !flag;
        if (!run)
            getThread().interrupt();
    }

    @Override
    public void run() {
        control.push(0, 0);

        Thread currentThread = Thread.currentThread();
        long lastWaitTimestamp = System.nanoTime();
        long lastMeasureTimestamp = System.nanoTime();

        while (run && !currentThread.isInterrupted()) {
            long stamp = System.nanoTime();
            float diff = (stamp - lastMeasureTimestamp) / 1_000_000f; // milliseconds
            if (diff == 0)
                diff = 0.01f;
            lastMeasureTimestamp = stamp;
            int actual = sensor.getRedFast();

            int error = Settings.PID_SENSOR_TARGET - actual;
            int turn = Math.round(pid.process(error, diff));

            if (Main.TRACK_MODE == TrackMode.RIGHT) {
                turn *= -1;
            }
            control.push(turn, error);

            lastWaitTimestamp = sleep(lastWaitTimestamp, Settings.PID1_SLEEP);
        }
        //logger.write();
    }
}
