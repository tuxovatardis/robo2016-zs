package tux.pid;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Datalog version 2
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.7
 */
public class Logger {
    private List<Entry> list;

    public Logger() {
        list = new LinkedList<>();
    }

    public void write() {
        StringBuilder builder = new StringBuilder();
        try (FileWriter stream = new FileWriter("/home/lejos/programs/datalog2.csv");
             BufferedWriter write = new BufferedWriter(stream)) {

            builder.append("timestamp").append(',')
                    .append("error").append(',')
                    .append("derivation").append(',')
                    .append("turn").append('\n');
            write.write(builder.toString());
            builder.setLength(0);

            for (Entry e : list) {
                builder.append(e.timestamp).append(',')
                        .append(e.error).append(',')
                        .append(e.derivative).append(',')
                        .append(e.turn).append('\n');
                write.write(builder.toString());
                builder.setLength(0);
            }
            write.flush();
            stream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void log(long timestamp, int error, float derivation, int turn) {
        list.add(new Entry(timestamp, error, derivation, turn));
    }

    private class Entry {
        long timestamp;
        int error;
        float derivative;
        int turn;

        private Entry(long timestamp, int error, float derivative, int turn) {
            this.timestamp = timestamp;
            this.error = error;
            this.derivative = derivative;
            this.turn = turn;
        }
    }
}
